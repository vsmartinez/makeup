package makeup;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.res.Resources.NotFoundException;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

public class MakeUp {

	public static void makeUp(Activity activity, String strConfig) {
		try {
			JSONObject jsonConfig = new JSONObject(strConfig);

			Iterator<String> iter = jsonConfig.keys();
			while (iter.hasNext()) {
				String id = iter.next();
				JSONObject obj = jsonConfig.getJSONObject(id);
				if (obj != null) {
					config(activity, id, obj);
				}
			}

		} catch (JSONException e) {
			Log.w("mUP", String.format("Invalid json format %s %s", strConfig, e.getMessage()));
		} catch (SecurityException e) {
			Log.w("mUP", String.format("Security Exception %s", e.getMessage()));
		}
	}

	private static void config(Activity activity, String idComponent, JSONObject jsonObject) {
		try {
			@SuppressWarnings("unchecked")
			Iterator<String> iter = jsonObject.keys();
			while (iter.hasNext()) {

				int i = activity.getResources().getIdentifier(idComponent, "id", activity.getPackageName());
				if (i == 0) { // if component was not found by its id,
								// trying
								// with the hashcode of id name.
					i = idComponent.hashCode();
				}

				String property = iter.next();
				String setter = setter(capitalize(property));
				Object value = jsonObject.get(property);
				View view = activity.findViewById(i);

				if (value.toString().startsWith("@")) {
					processMethod(view, setter, resolveResource(activity, value.toString()));
				} else {
					processMethod(view, setter, value);
				}

			}
		} catch (JSONException e) {
			Log.w("mUP", String.format("Invalid json %s", e.getMessage()));
		}
	}

	private static void processMethod(View view, String setter, Object value) {

		try {
			if (value instanceof JSONArray) {
				JSONArray ja = (JSONArray) value;
				Class<?>[] os = new Class[ja.length()];
				Object[] ov = new Object[ja.length()];
				for (int j = 0; j < ja.length(); j++) {
					ov[j] = ja.get(j);
					os[j] = ja.get(j).getClass();
				}

				Method method = matchMethod(view.getClass(), setter, os);

				if (method == null) {
					Log.w("mUP",
							String.format("Method %s with parameter types %s not found", setter, Arrays.toString(os)));
					return;
				}

				Object[] nov = caster(ov, method.getParameterTypes());
				method.invoke(view, nov);
			} else {
				Method method = findMethod(view.getClass(), setter, value.getClass());

				if (method == null) {
					Log.w("mUP", String.format("Method %s with parameter type %s not found", setter, value.getClass()
							.getName()));
					return;
				}

				Object[] nov = caster(new Object[] { value }, method.getParameterTypes());
				method.invoke(view, nov[0]);
			}
		} catch (IllegalAccessException e) {
			Log.w("mUP", String.format("Illegal Access %s", e.getMessage()));
		} catch (JSONException e) {
			Log.w("mUP", String.format("Invalid json format %s", e.getMessage()));
		} catch (InvocationTargetException e) {
			Log.w("mUP", String.format("Invalid target %s", e.getMessage()));
		} catch (SecurityException e) {
			Log.w("mUP", String.format("Security Exception %s", e.getMessage()));
		} catch (IllegalArgumentException e) {
			Log.w("mUP", String.format("Invalid argument %s", e.getMessage()));
		}
	}

	private static int resolveResource(Activity activity, String value) {

		String[] split = value.split("\\.");
		String clazz = null;
		String field = null;

		try {

			if (split[0].startsWith("@android")) {
				if (split[1].equals("R")) {
					clazz = "android.R$" + split[2];
					field = split[3];
					return activity.getResources().getInteger((Integer) Class.forName(clazz).getField(field).get(null));
				} else if (split[1].equals("view")) {
					clazz = "android.view." + split[0].substring(1);
					field = split[1];
					return (Integer) Class.forName(clazz).getField(field).get(null);
				}

			} else {
				if (split[0].startsWith("@R")) {
					int resourceId = activity.getResources().getIdentifier(split[2], split[1],
							activity.getPackageName());

					if ("drawable".equals(split[1])) {
						return resourceId;
					}

					TypedValue t = new TypedValue();
					activity.getResources().getValue(resourceId, t, true);
					return t.data;
				}
			}
		} catch (IllegalAccessException e) {
			Log.w("mUP", String.format("Illegal Access %s", e.getMessage()));
		} catch (SecurityException e) {
			Log.w("mUP", String.format("Security Exception %s", e.getMessage()));
		} catch (IllegalArgumentException e) {
			Log.w("mUP", String.format("Invalid argument %s", e.getMessage()));
		} catch (NotFoundException e) {
			Log.w("mUP", String.format("Resource not found %s %s", value, e.getMessage()));
		} catch (NoSuchFieldException e) {
			Log.w("mUP", String.format("Field %s doesn't exist", field));
		} catch (ClassNotFoundException e) {
			Log.w("mUP", String.format("Class %s doesn't exist", clazz));
		}

		return 0;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	private static Map<Class<?>, List<Class<?>>> alias = new HashMap() {

		{
			put(Integer.class, new ArrayList<Class<?>>() {
				{
					add(int.class);
					add(float.class);
					add(Float.class);
				}
			});

			put(Float.class, new ArrayList<Class<?>>() {
				{
					add(float.class);
					add(Float.class);
					add(Integer.class);

				}
			});

			put(Double.class, new ArrayList<Class<?>>() {
				{
					add(double.class);
					add(float.class);

				}
			});

			put(String.class, new ArrayList<Class<?>>() {
				{
					add(CharSequence.class);
				}
			});

			put(Boolean.class, new ArrayList<Class<?>>() {
				{
					add(boolean.class);
				}
			});
		}
	};

	private static Method findMethod(Class<?> target, String name, Class<?> parameterClazz) {

		List<Class<?>> types = alias.get(parameterClazz);
		for (Class<?> type : types) {
			try {
				return target.getMethod(name, type);
			} catch (SecurityException se) {
				// Keep going with the other alias
			} catch (NoSuchMethodException nsme) {
				// Keep going with the other alias
			}
		}

		return null; // No method found
	}

	private static Method matchMethod(Class<?> target, String name, Class<?>[] parametersTarget) {
		for (Method method : target.getMethods()) {
			if (!method.getName().equals(name)) {
				continue;
			}

			Class<?>[] parameters = method.getParameterTypes();

			if (parameters.length != parametersTarget.length) {
				continue;
			}

			boolean compatible = true;
			for (int i = 0; i < parameters.length; i++) {

				compatible &= isCompatible(parameters[i], parametersTarget[i]);

				if (!compatible) {
					break;
				}

			}

			if (compatible) {
				return method;
			}
		}
		return null;
	}

	private static boolean isCompatible(Class<?> target, Class<?> source) {
		return alias.get(source).indexOf(target) > -1;
	}

	private static Object[] caster(Object[] source, Class<?>[] targetType) {
		Object[] o = new Object[targetType.length];
		for (int i = 0; i < targetType.length; i++) {
			if (source[i] instanceof Double && targetType[i].getName().equals("float")) {
				o[i] = ((Double) source[i]).floatValue();
			} else {
				o[i] = source[i];
			}
		}
		return o;
	}

	private static String capitalize(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private static String setter(String name) {
		return String.format("set%s", name);
	}
}
