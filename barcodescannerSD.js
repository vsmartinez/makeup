var barcodeScannerSD = {

	openScanner : function(successCallback, failureCallback) {
		return cordova
				.exec(
						successCallback,
						failureCallback,
						"BarcodeCordova",
						"start",
						[
								"IRLYnpDMEeKdWr/b03mBTnmN051WgQxDVi7r6zsvDhI",
								{
									"beep" : true,
									"1DScanning" : true,
									"2DScanning" : true,
									"scanningHotspot" : "0.5/0.5",
									"vibrate" : true,
									"textForInitialScanScreenState" : "Align code with box",
									"textForBarcodePresenceDetected" : "Align code and hold still",
									"textForBarcodeDecodingInProgress" : "Decoding",
									"searchBarActionButtonCaption" : "Go",
									"searchBarCancelButtonCaption" : "Cancel",
									"searchBarPlaceholderText" : "Scan barcode or enter it here",
									"toolBarButtonCaption" : "Cancel",
									"minSearchBarBarcodeLength" : 8,
									"maxSearchBarBarcodeLength" : 15,
									"drawViewfinderText" : false
								},
								{
									"idBack" : "btnBack",
									"btnHelp" : {
										"id" : "btnHelp",
										"label" : "Help",
										"visible" : true
									},
									"barLabel" : "Capture",
									"defaultTab" : "tabCamera",
									"tabs" : [
											{
												"label" : "Camera",
												"id" : "tabCamera",
												"visible" : true,
												"topLegend" : "Point the camera to the product bardcode in order to scan the barcode.",
												"buttons" : [ {
													"label" : "Type Barcode",
													"id" : "btnTypeBarcode",
													"visible" : true
												}, {
													"label" : "No Barcode",
													"id" : 'btnNoBarcode',
													"visible" : true
												} ]
											},
											{
												"label" : "Clicker",
												"id" : "tabClicker",
												"visible" : true,
												"topLegend" : "Point your laser scanner device to de product barcode.",
												"buttons" : [ {
													"label" : "Other",
													"id" : "btnOther",
													"visible" : true
												} ]
											},
											{
												"label" : "Type",
												"id" : "tabTypeBarcode",
												"visible" : true,
												"topLegend" : "Type in the product's barcode.",
												"buttons" : [ {
													"label" : "Type Barcode",
													"id" : "btnTypeBarcode",
													"visible" : true
												}, {
													"label" : "No Barcode",
													"id" : 'btnNoBarcode',
													"visible" : true
												}, {
													"label" : "Other",
													"id" : "btnOther",
													"visible" : true
												} ]
											} ],

									"config" : {
										"btnOther" : {
											"text" : "Te engaño con esta otra",
											"backgroundResource" : "@R.drawable.light",
											"textColor" : "@R.color.finder_box",
											"padding" : [ 50, 50, 50, 50 ]
										},
										"button_help" : {
											"text" : "Te engaño con esta",
											"width" : 500,
											"textColor" : "@R.color.finder_decoded",
											"textSize" : 9.0,
											"backgroundColor" : "@R.color.finder_box",
											"gravity" : "@Gravity.LEFT",
											"visibility" : "@View.VISIBLE",
											"shadowLayer" : [ 5.5, 2.1, 2.3, 1 ],
											"textSize" : [ 2, 12.8 ]
										},
										"title" : {
											"text" : "Te cambie el titulo",
											"textSize" : 10.2,
										},
										"button_continer" : {
											"backgroundColor" : "@android.R.color.white",
											"padding" : [ 100, 100, 100, 100 ]
										},
										"root" : {
											"padding" : [ 100, 0, 100, 0 ]
										},
										"title_scan" : {
											"textSize" : 20
										}

									}
								} ]);
	}

};

if (!window.plugins) {
	window.plugins = {};
}
if (!window.plugins.BarcodeScannerSD) {
	window.plugins.BarcodeScannerSD = barcodeScannerSD;
}

console.log("barcodescannerSD loaded");